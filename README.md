# sf_etl_repo
## Описание


sf_etl_repo - основной репозиторий всех ETL-процессов в Skillfactory.  

- Язык программирования - Python  
- Оркестратор - Apache Airflow  
- Для трансформации данных - data build tool (dbt cli)


## Билд и запуск
Первичный 

- Выполнить ```git pull``` репозитория на linux машину.  
Например ```git pull origin master```;  
- Переместить ```Dockerfile``` и ```docker-compose.yaml``` из папки репозитория на директорю выше, а так же скопировать из ```creds_for_airflow``` ```.env``` файл и расположить его по соседству с sf_etl_repo.  
В итоге у Вас должно получиться:
> /sf_etl_repo  
> Dockerfile  
> docker-compose.yaml  
> .env


- Переименовать файл в локальном репозитории ```config_for_airflow.py``` в ```config.py```;
- Скопировать файлы ```ga_secret.json, gbq-secret.json, google_secret.json``` из папки ```creds_for_airflow``` в папку ```sf_etl_repo```;
- Запустить команду ```docker-compose up```;
- Дождаться завершения билда контейнеров;
- Перейти по ссылке http://130.193.51.135:8080/home, ввести логин и пароль от пользователя Apache Airflow, указанного в ```.env``` файле.

Перезапуск Apache Airflow  

- Выполнить ```docker-compose restart```.

Добавление новых Python библиотек/пакетов в контейнеры Apache Airflow  

- Добавить необходимые Python библиотеку в Dockerfile в строку с ```RUN pip install``` через пробел от последней библиотеки;  
- Добавить необходимые Linux пакеты в Dockerfile в строку с ```RUN``` находящуюся между строками ```USER root``` и ```USER airflow```. Разделителями являются ```'&&'``` и ```'\'```;  
- Выполнить ```docker-compose down```;  
- Выполнить ```time DOCKER_BUILDKIT=1 docker-compose up --build```.  

## Добавление новых credentials путем добавления новых переменных окружения
- Модификацировать .env файл.  
Согласно документации Apache Airflow все переменные окружени добавляемые в Docker контейнеры с Airflow должны начинаться на ```AIRFLOW_VAR_```.  
Например ```AIRFLOW_VAR_YANDEX_TRACKER_TOKEN```;
- Модифицировать ```config.py``` в локальном репозитории, то есть записать в Python переменную данные из переменной окружения:
```
YANDEX_TRACKER_TOKEN = os.environ["AIRFLOW_VAR_YANDEX_TRACKER_TOKEN"]
```
- Перезапустить контейнеры командами 
```
docker-compose down
docker-compose up
```

glpat-s-35HJsowRNeYxRXJJR4