import logging
from inspect import currentframe
from typing import Dict, List

import pandas as pd
from config import DWH_CONN, PATH, YANDEX_METRIKA_ACCESS_TOKEN
from sqlalchemy import create_engine

from .api import LogsAPI

logger = logging.getLogger(__name__)
engine = create_engine(DWH_CONN)

HITS_SCHEMA = "marketing"
HITS_BUFFER_TABLE = "yandex_metrika_hits_buffer"

URL_PATTERN = r"^https?:\/\/?(?:\w+\.)*?(skillfactory\.ru|contented\.ru)\/?.*"
DEVICES_MAP = {"1": "desktop", "2": "mobile", "3": "tablet", "4": "TV"}


def transform_hits(
    counter_id: int, data: List[dict], fields: Dict[str, str]
) -> pd.DataFrame:
    """Transforms the hits data into a format ready for loading into the buffer table

    Args:
        counter_id: Counter ID, you can find it in Yandex Metrika dashboard
        data: A list of dictionaries representing the hits data
        fields: Map of Yandex Metrika fields and their names in the table
    """
    hits = pd.DataFrame(data)
    hits = hits[hits["ym:pv:URL"].str.contains(URL_PATTERN, case=False, na=False)]

    hits["ym:pv:deviceCategory"] = hits["ym:pv:deviceCategory"].map(DEVICES_MAP)
    hits["ym:pv:dateTime"] = pd.to_datetime(hits["ym:pv:dateTime"]).dt.tz_localize(
        "Europe/Moscow"
    )

    hits.rename(columns=fields, inplace=True)
    hits["counter_id"] = counter_id

    return hits


def load_hits_to_target():
    """Loads hits data to the PostgreSQL dwh database"""
    frame = currentframe().f_code.co_name
    with open(
        f"{PATH}sql/yandex_metrika/insert_hits_into_yandex_metrika_hits.sql",
        "r",
        encoding="utf-8",
    ) as file:
        INSERT = file.read()

    with engine.begin() as conn:
        conn.execute(INSERT)
        conn.execute(f"DROP TABLE IF EXISTS {HITS_SCHEMA}.{HITS_BUFFER_TABLE};")
        logger.info(f"{frame} | Hits loaded successfully. The buffer table dropped.")


def import_ym_hits_to_dwh(
    counter_id: int, from_dt: str, to_dt: str, fields: Dict[str, str]
):
    """Imports hits data from Yandex Metrika to PostgreSQL dwh database

    Args:
        counter_id: Counter ID, you can find it in Yandex Metrika dashboard
        from_dt: Date from which to get data, format: YYYY-MM-DD
        to_dt: Date to which to get data, format: YYYY-MM-DD
        fields: Map of Yandex Metrika fields to get and their names in the table
    """
    frame = currentframe().f_code.co_name
    logger.info(
        f"{frame} | Start importing {counter_id=} hits from {from_dt} to {to_dt}..."
    )

    # Get hits from Yandex Metrika
    logsapi = LogsAPI(access_token=YANDEX_METRIKA_ACCESS_TOKEN, counter_id=counter_id)
    hits = logsapi.get(
        source="hits", from_dt=from_dt, to_dt=to_dt, fields=list(fields.keys())
    )

    # Transform hits data
    hits_df = transform_hits(counter_id=counter_id, data=hits, fields=fields)

    # Load hits data into the buffer table
    hits_df.to_sql(
        schema=HITS_SCHEMA,
        name=HITS_BUFFER_TABLE,
        index=False,
        if_exists="replace",
        con=engine,
    )
    logger.info(
        f"{frame} | Hits imported successfully. " f"The hits buffer table prepared."
    )
