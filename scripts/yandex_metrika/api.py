import logging
from inspect import currentframe
from typing import List

from tapi_yandex_metrika import YandexMetrikaLogsapi

logger = logging.getLogger(__name__)


class LogsAPI:
    def __init__(self, access_token: str, counter_id: int, wait_report: bool = True):
        """
        Args:
            access_token: Access token for Yandex Metrika
            counter_id: Counter ID, you can find it in Yandex Metrika dashboard
            wait_report: Download the report when it will be created
        """
        self.client = YandexMetrikaLogsapi(
            access_token=access_token,
            default_url_params={"counterId": counter_id},
            wait_report=wait_report,
        )

    def _check(self, params: dict) -> bool:
        """Check the possibility of creating a report"""
        result = self.client.evaluate().get(params=params)
        return result["log_request_evaluation"]["possible"]

    def _request_id(self, params: dict) -> int:
        """Order a report and get request ID"""
        info = self.client.create().post(params=params)
        return info["log_request"]["request_id"]

    def get(
        self, source: str, from_dt: str, to_dt: str, fields: List[str]
    ) -> List[dict]:
        """Extract hits or visits data from Yandex Metrika

        Args:
            source: It can be "hits" or "visits"
            from_dt: Date from which to get hits data, format: YYYY-MM-DD
            to_dt: Date to which to get hits data, format: YYYY-MM-DD

                WARNING: to_dt must be < current_date. For example:
                from_dt=2022-02-02 and to_dt=2022-02-02 you will get data for 2022-02-02

            fields: List of fields to get
        """
        frame = currentframe().f_code.co_name
        params = {
            "fields": ",".join(fields),
            "source": source,
            "date1": from_dt,
            "date2": to_dt,
        }

        # Check the possibility of creating a report
        if not self._check(params=params):
            raise Exception(
                f"{frame} | Impossible to create a report. "
                "Please check the ACCESS TOKEN validity and COUNTER ID.\n"
                "Also check the date range, how big it is and its correctness. "
                "Or try again after several minutes."
            )

        # Download the report
        request_id = self._request_id(params=params)
        report = self.client.download(requestId=request_id).get()

        data = [row for row in report().iter_dicts()]

        # Delete the report after successful extracting
        self.client.clean(requestId=request_id).post()
        logger.info(
            f"{frame} | Data extracted successfully: rows={len(data)}. "
            "Report deleted."
        )

        return data
