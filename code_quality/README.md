Formatting & Linting
---

Для удобства работы с форматированием и линтингом Python кода мы будем использовать `Makefile`. Если вы пользователь Mac OS системы, то у вас уже есть всё необходимое для работы с ним, а если ваша система Windows, то можно рассмотреть следующий гайд по установке необходимых зависимостей:

> <details>
>   <summary>Makefile для Windows</summary>
> 
> 1. Запустите команду установки менеджера пакетов `choco`, для последующей работы с `Makefile`:
> 
> ```shell
> Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
> ```
> 
> 2. Затем, в том же терминале, установите модуль `make`:
> 
> ```
> choco install make
> ```
> > После чего проверьте установку командой `make -v`
> </details>

---

Перед тем как начинать форматирование и линтинг, используя  `Makefile`, нам необходимо в активированную виртуальную среду разработки (venv) установить менеджер пакетов `poetry`.

> <details>
>   <summary>У меня нет виртуальной среды разработки</summary>
> 
> 1. Откройте терминал в корне вышего проекта и запустите команду:
> 
> * Mac OS: `python3 -m venv venv`
> * Windows: `python -m venv venv`
> 
> 2. После установки, активируйте среду `venv` командой:
> 
> * Mac OS ```source venv/bin/activate```
> * Windows ```venv\Scripts\activate```
> > Если, при активации в Windows, произошла ошибка, то откройте **admin** PowerShell и выполните команду:
> > ```
> > Set-ExecutionPolicy RemoteSigned
> > ```
> > Введите `Y` и попробуйте активировать `venv` снова
> 
> </details>


Установить `poetry` можно командой:

```
pip install poetry
```

Затем нам необходимо проинициализировать библиотеки `black`, `isort` и `flake8`, запустив команду:

```
make init-cq
```

---

Если установка прошла успешно, то мы можем начинать использовать команду автоматического форматировани и линтинга кода, которая коснется Python скриптов в папках `dags` и `scripts`:

```
make format
```
