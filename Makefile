# Code Quality modules initialization
init-cq:
	cd code_quality && poetry install --no-root

# Formatting & Linting
format:
	cd code_quality && poetry run black --config=pyproject.toml ../scripts/ ../dags/
	cd code_quality && poetry run isort --settings-file pyproject.toml ../scripts/ ../dags/
	cd code_quality && poetry run flake8 --config=.flake8 ../scripts/ ../dags/

check:
	cd code_quality && poetry run black --config=pyproject.toml --check ../scripts/ ../dags/
	cd code_quality && poetry run isort --settings-file pyproject.toml --check ../scripts/ ../dags/
	cd code_quality && poetry run flake8 --config=.flake8 ../scripts/ ../dags/
