from datetime import datetime

from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python import BranchPythonOperator, PythonOperator
from scripts.tools.db_pg_tools import drop_partition_by_month_old
from scripts.tools.time_messenger_tools import task_fail_time_alert
from scripts.yandex_metrika.utils import import_ym_hits_to_dwh, load_hits_to_target

FIELD_COLUMN_MAP = {
    "ym:pv:watchID": "hit_id",
    "ym:pv:counterUserIDHash": "counter_user_id",
    "ym:pv:clientID": "clientid",
    "ym:pv:dateTime": "ts",
    "ym:pv:UTMSource": "utm_source",
    "ym:pv:UTMMedium": "utm_medium",
    "ym:pv:UTMCampaign": "utm_campaign",
    "ym:pv:UTMContent": "utm_content",
    "ym:pv:UTMTerm": "utm_term",
    "ym:pv:URL": "url",
    "ym:pv:browser": "browser",
    "ym:pv:regionCountry": "country",
    "ym:pv:regionCity": "city",
    "ym:pv:deviceCategory": "device_category",
}

COUNTERS = {"SkillFactory": 38813825, "Contented": 45562158}

default_args = {
    "owner": "SkillFactory",
    "depends_on_past": False,
    "retries": 1,
    "on_failure_callback": task_fail_time_alert,
}


def _check_old_hits_partition(logical_date):
    if logical_date.strftime("%d") == "02":
        return "drop_old_hits_partition"
    return "skip_drop_partition"


with DAG(
    dag_id="import_yandex_metrika_hits",
    description="Imports hits data from SkillFactory and Contented platforms via Yandex Metrika",
    tags=["yandex_metrika", "hits"],
    start_date=datetime(2024, 1, 1),
    schedule_interval="0 0 * * *",
    catchup=True,
    max_active_runs=1,
    default_args=default_args,
) as dag:
    data_interval_start = "{{ data_interval_start.strftime('%Y-%m-%d') }}"

    import_skillfactory_hits = PythonOperator(
        task_id="import_skillfactory_hits",
        python_callable=import_ym_hits_to_dwh,
        op_kwargs={
            "counter_id": COUNTERS["SkillFactory"],
            "from_dt": data_interval_start,
            "to_dt": data_interval_start,
            "fields": FIELD_COLUMN_MAP,
        },
    )

    import_contented_hits = PythonOperator(
        task_id="import_contented_hits",
        python_callable=import_ym_hits_to_dwh,
        op_kwargs={
            "counter_id": COUNTERS["Contented"],
            "from_dt": data_interval_start,
            "to_dt": data_interval_start,
            "fields": FIELD_COLUMN_MAP,
        },
    )

    load_hits_from_skillfactory_buffer_to_target = PythonOperator(
        task_id="load_hits_from_skillfactory_buffer_to_target",
        python_callable=load_hits_to_target,
        params={"task_alert_priority": 1},
    )

    load_hits_from_contented_buffer_to_target = PythonOperator(
        task_id="load_hits_from_contented_buffer_to_target",
        python_callable=load_hits_to_target,
        params={"task_alert_priority": 1},
    )

    check_old_hits_partition = BranchPythonOperator(
        task_id="check_old_hits_partition",
        python_callable=_check_old_hits_partition,
        provide_context=True,
        start_date=datetime(2024, 7, 1),
    )

    drop_old_hits_partition = PythonOperator(
        task_id="drop_old_hits_partition",
        python_callable=drop_partition_by_month_old,
        op_kwargs={
            "schema": "marketing",
            "parent_table": "yandex_metrika_hits",
            "month_old": 6,
        },
        start_date=datetime(2024, 7, 1),
    )

    skip_drop_partition = DummyOperator(
        task_id="skip_drop_partition", start_date=datetime(2024, 7, 1)
    )

    (
        import_skillfactory_hits
        >> load_hits_from_skillfactory_buffer_to_target
        >> import_contented_hits
        >> load_hits_from_contented_buffer_to_target
        >> check_old_hits_partition
        >> [drop_old_hits_partition, skip_drop_partition]
    )
